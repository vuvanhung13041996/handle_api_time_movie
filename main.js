var content = document.getElementById("content")
var datajson = `https://teachingserver.onrender.com/FastCinema`

const filltable = () => {
    content.innerHTML = "";
    fetch(datajson)
        .then(function (response) {
            console.log(response);
            return response.json()
        })
        .then(function (data) {
            content.innerHTML +=
            data.map(function (dataitem) {
                return `<div>
                            <h1>${dataitem.name}</h1>
                            <hr/>
                            ${dataitem.dates.map(function (dateojb) {
                                return `<h3>${dateojb.showDate}</h3>
                                ${dateojb.bundles.map(function (bundlesojb) {
                                    return `<h4>${bundlesojb.version}</h4>
                                    ${bundlesojb.sessions.map(function (sessionsojb) {
                                        return `<span>${sessionsojb.showTime}</span>`
                                    }).join("")}
                                    `
                                }).join("")}`
                            })}
                        </div>`
            }).join("")
        });
}
filltable()